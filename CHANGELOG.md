# Changelog

## Unreleased

- Data will now be fetched with the BitTorrent DHT

This changelog was started after the initial release, version 0.1.0.
