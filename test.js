import { describe, it, before, after, beforeEach, afterEach } from "node:test";
import * as assert from "node:assert/strict";
import * as path from "node:path";
import { tmpdir } from "node:os";
import * as fs from "node:fs/promises";
import { register } from "node:module";
import { Server } from "bittorrent-tracker";
import WebTorrent from "webtorrent";

describe("torrent-import", () => {
  // Hook setup

  before(() => {
    register("./hooks.mjs", import.meta.url);
  });

  // Tracker

  let tracker;

  const getTrackerUrl = () => {
    const result = new URL("http://localhost/announce");
    result.port = tracker.http.address().port;
    return result;
  };

  beforeEach((_t, done) => {
    tracker = new Server({ udp: false, http: true, ws: false });
    tracker.listen(0, "localhost", done);
  });

  afterEach((_t, done) => {
    tracker.close(done);
  });

  // Seeder

  let seeder;
  let torrent;

  beforeEach((_t, done) => {
    const source = "export function foo() { return 'bar' }";

    seeder = new WebTorrent({ dht: false });
    seeder.seed(
      Buffer.from(source, "utf8"),
      {
        name: "greet.js",
        announceList: [[getTrackerUrl().href]],
      },
      (t) => {
        torrent = t;
        done();
      },
    );
  });

  afterEach((_t, done) => {
    seeder.destroy(done);
  });

  // Temporary files

  const temporaryFiles = [];

  const writeTempFile = async (filename, data) => {
    const dir = await fs.mkdtemp(path.join(tmpdir(), "torrent-import-"));
    const file = path.join(dir, filename);
    fs.writeFile(file, data);
    temporaryFiles.push(file);
    return file;
  };

  after(async () => {
    await Promise.all(temporaryFiles.map((f) => fs.unlink(f)));
  });

  // Tests

  it("can import from a .torrent file", async () => {
    const torrentFilePath = await writeTempFile(
      "greet.torrent",
      torrent.torrentFile,
    );

    const mod = await import(torrentFilePath);
    assert.equal(mod.foo(), "bar");
  });

  it("can import from a magnet URI", async () => {
    const mod = await import(torrent.magnetURI);
    assert.equal(mod.foo(), "bar");
  });
});
