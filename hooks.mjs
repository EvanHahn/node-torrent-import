import * as path from "node:path";
import { pathToFileURL } from "node:url";
import * as fs from "node:fs/promises";
import { tmpdir } from "node:os";
import WebTorrent from "webtorrent";
import parseTorrent from "parse-torrent";
import createDebug from "debug";
import findCacheDirectory from "find-cache-dir";

const debug = createDebug("torrent-import");

// TODO: Can this be made more robust?
const isTorrent = (str) => str.endsWith(".torrent");

const isMagnet = (str) => {
  try {
    return new URL(str).protocol === "magnet:";
  } catch (_err) {
    return false;
  }
};

const getInfoHash = async (specifier) => {
  if (isTorrent(specifier)) {
    const torrentData = await fs.readFile(specifier);
    return (await parseTorrent(torrentData)).infoHash;
  }

  if (isMagnet(specifier)) {
    return (await parseTorrent(specifier)).infoHash;
  }

  return undefined;
};

const getCacheDirectory = () =>
  findCacheDirectory({ name: "torrent-import" }) ??
  path.join(tmpdir(), "torrent-import");

export const resolve = async (specifier, context, nextLoad) => {
  const infoHash = await getInfoHash(specifier);
  if (!infoHash) {
    return nextLoad(specifier, context);
  }

  debug(`Importing ${specifier}...`);

  const downloadPath = path.join(getCacheDirectory(), infoHash);

  debug(`Downloading ${specifier} into ${downloadPath}...`);
  const client = new WebTorrent();
  const torrent = await new Promise((resolve, reject) => {
    client.add(specifier, { path: downloadPath }, (torrent) => {
      torrent.on("error", reject);
      torrent.on("done", () => {
        resolve(torrent);
      });
    });
  });
  debug(`Downloaded ${specifier}.`);

  const jsFiles = torrent.files.filter((f) => f.path.endsWith(".js"));
  if (jsFiles.length !== 1) {
    throw new Error(`${specifier} must contain exactly one .js file.`);
  }
  const file = jsFiles[0];
  const filePath = path.join(torrent.path, file.path);
  const fileUrl = pathToFileURL(filePath).toString();

  debug(`Destroying client after downloading ${specifier}...`);
  await new Promise((resolve, reject) => {
    client.destroy((err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
  debug(`Finished downloading ${specifier}. Returning ${fileUrl}.`);

  return { format: "module", shortCircuit: true, url: fileUrl };
};
