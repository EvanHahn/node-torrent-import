# Node torrent import

A proof-of-concept showing how to download Node modules over BitTorrent.

_This is not currently recommended for production use (see below)._

## Quick start

1. Install `torrent-import`:

   ```sh
   npm install torrent-import
   ```

1. Create a file that imports from a local torrent file.

   ```javascript
   import { greet } from "./greet.js.torrent";
   console.log(greet());
   ```

   You can also use a `magnet:` URI instead of a torrent path.

   All files must use ES Modules. CommonJS is not supported (yet?).

1. Run Node with the `--import torrent-import` flag added.

   ```sh
   node --import torrent-import my-app.js
   # => Hello world!
   ```

See this repo's `example/` folder for a runnable demo.

## How it works

Node supports ["Customization Hooks"](https://nodejs.org/docs/latest-v21.x/api/module.html#customization-hooks), which let you customize how modules are imported. `torrent-import` takes advantage of this feature.

Effectively, here's what this module does:

1. Adds an import hook that activates when importing a `.torrent` file or a `magnet:` URI.
1. When activated, uses [WebTorrent](https://www.npmjs.com/package/webtorrent) to download the torrent.
1. Grabs the first `.js` file in the torrent and returns it, as if you'd imported it from the local file system.

It's a _little_ more complicated (there's some caching, some debug logging, and some cleanup), but that's the idea. Read more about it [in my blog post](https://evanhahn.com/node-torrent-import/).

As of this writing, Customization Hooks are unstable in Node, but it's at the "release candidate" [stage](https://nodejs.org/docs/latest-v21.x/api/documentation.html#stability-index).

## Why do this?

- The primary reason: fun!
- Demonstrate that it's possible to import code in a decentralized way, without relying on centralized registries like npm (run by Microsoft).
- Kick the tires on a new Node feature to see how it works.

## Why is this bad?

I do not recommend this for production use for several reasons:

- Customization Hooks, the main feature this module uses, are currently unstable in Node.
- Most torrents use SHA-1 for data integrity, but [it's possible to break SHA-1](https://www.zdnet.com/article/sha-1-collision-attacks-are-now-actually-practical-and-a-looming-danger/). That means someone could run different code than you expect! (This threat is mitigated if you use [BitTorrent v2](https://www.bittorrent.org/beps/bep_0052.html).)
- Not isomorphic. In other words, it cannot run in the browser or other JavaScript runtimes.
- Many tools do not support this: TypeScript, "go to definition" in your editor, many more.
- Depends on seeder availability. If nobody's seeding the torrent, you can't run your code.
- The word "torrent" makes some people bristle, as it is often associated with illegal activity. BitTorrent is just a protocol like HTTP&mdash;there's nothing inherently illegal about it&mdash;but you don't see corporate firewalls blocking HTTP.
- This is a proof of concept and is rough. Error handling doesn't work well, for example.

There are probably other problems with this idea.
